//var JpegTran = require('jpegtran'),
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');




	new Promise(async () => {
	try {
		const files = await imagemin(['images/*.{jpg,jpeg,png}'], 'build', {
			plugins: [
				imageminMozjpeg({
					quality: 90,

				}),
				imageminPngquant({
					quality: [0.6, 0.8],
					strip: true
				})
			]
		});
	} catch(error)  {
		console.log('await error: ', error.message);
	};
	//=> [{data: <Buffer 89 50 4e …>, path: 'build/images/foo.jpg'}, …]
}).catch(error => { console.log('caught', error.message); });

